from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Hat(models.Model):

    name = models.CharField(max_length=40)
    fabric = models.CharField(max_length=40)
    color = models.CharField(max_length=40)
    pic_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name", "fabric", "color")
