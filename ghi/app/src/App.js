import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoesList'
import HatsList from './HatsList';
import HatForm from './HatForm'
import ShoeForm from './ShoeForm';
import ShoesDeleteForm from './DeleteShoeForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='shoes'>
            <Route index element={<ShoeList/>}/>
            <Route path="new" element={<ShoeForm/>} />
            <Route path='delete' element={<ShoesDeleteForm/>}/>
            </Route>
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatForm/>} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
