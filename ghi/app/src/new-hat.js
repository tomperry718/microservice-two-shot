window.addEventListener('DOMContentLoaded', async () => {
    const hatsUrl = 'http://localhost:8000/api/hats/';
    const response = await fetch(hatsUrl); 
    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('hat');
            for (let hat of data.hats) {
                const option = document.createElement('option');
                option.value = hat.id;
                option.innerHTML = hat.name;
                selectTag.appendChild(option);
                }
            }
    const formTag = document.getElementById('create-hat-form');
    const selectTag = document.getElementById('hat');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData.entries()));  
        const hatId = selectTag.options[selectTag.selectedIndex].value;
        const hatsUrl = `http://localhost:8000/api/hats/${hatId}/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
            'Content-Type': 'application/json',
            },
            };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newHat = await response.json();
            console.log(newHat);
        }
    });
});
