import {useEffect, useState } from 'react'

function ShoeForm() {
    const [formData, setFormData] = useState({
        name: '',
        manufacturer: '',
        color: '',
        pic_url: '',
        bin:  '',
    })
    const handleSubmit = async (event) => {
        event.preventDefault()

        console.log('formData:', formData);

        const shoesurl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }

        const response = await fetch(shoesurl, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                manufacturer: '',
                color: '',
                pic_url: '',
                bin:  '',
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputShoe = e.target.name

        setFormData({
            ...formData,
            [inputShoe]: value,
        })
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Shoe</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-form">

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Pic_url" required type="text"name="pic_url" id="pic_url" className="form-control" />
                            <label htmlFor="pic_url">Pic_url</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="bin" required type="text" name="bin" id="bin" className="form-control" />
                            <label htmlFor="bin">Shoe Bin</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default ShoeForm;
