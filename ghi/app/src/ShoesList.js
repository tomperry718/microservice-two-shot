import {useEffect, useState} from "react"

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const shoesUrl = 'http://localhost:8080/api/shoes/'
    const response = await fetch(shoesUrl)

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  console.log("shoes:", shoes);

    return (
      <table>
        <thead>
          <tr>
            <th>Shoe List</th>
            <th>Shoe Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.name}</td>
                <td>{shoe.manufacturer}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
