import React, {useEffect, useState} from "react"

function HatsList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const hatsUrl = 'http://localhost:8090/api/hats/'
    const response = await fetch(hatsUrl)
    
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Hat List</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.location}>
              <td>{ hat.name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
