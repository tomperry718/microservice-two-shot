import { useState } from "react";

function HatForm() {
    const [formData, setFormData] = useState({
        name: '',
        fabric: '',
        color: '',
        pic_url: '',
        location:  '',
    })
    const handleSubmit = async (event) => {
        event.preventDefault()

        const hatsUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": 'application/json',
            },
        }

        const response = await fetch(hatsUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                fabric: '',
                color: '',
                pic_url: '',
                location:  '', 
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value
        const inputHat = e.target.name

        setFormData({
            ...formData,
            [inputHat]: value
        })
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                        <form onSubmit={handleSubmit} id="create-hat-form">
            
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Hat Name</label>
                        </div>
            
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Fabric" required type="text" name="Fabric" id="Fabric" className="form-control" />
                            <label htmlFor="starts">Hat Fabric</label>
                        </div>
            
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" required type="text" name="Color" id="Color" className="form-control" />
                            <label htmlFor="ends">Hat Color</label>
                        </div> 

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="PictureUrl" name="PictureUrl" id="PictureUrl" className="form-control" />
                            <label htmlFor="max_presentations">Picture Url</label>
                        </div>
            
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="location" required type="number" name="location" id="location" className="form-control" />
                            <label htmlFor="max_attendees">Hat Location</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm