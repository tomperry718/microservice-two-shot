# Wardrobify

Team:

* Person 1 - Tamekia - Hats microservice
* Person 2 - Tom - Shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
I created a shoe model that taking in characteristics of a shoe and has a foreign key "bin" that resides in the wardrobe microservice.


## Hats microservice
The hats models look at the name, fabric, color, url and location of a hat. It corresponds to the wardrobe microservice using the location as the primary key to track where the hat is located within the wardrobe. 
