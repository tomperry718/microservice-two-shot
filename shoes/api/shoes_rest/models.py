from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Shoe(models.Model):

    name = models.CharField(max_length=40)
    manufacturer = models.CharField(max_length=40)
    color = models.CharField(max_length=40)
    pic_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.name}"

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name", "manufacturer", "color")
